FROM openjdk:11-jdk-slim

LABEL maintainer="dini rasyiddah <diniirasyiddah@gmail.com>"

WORKDIR /app

COPY target/ecommerce.jar app/ecommerce.jar

EXPOSE 8080

CMD ["java", "-jar", "app/ecommerce.jar"]