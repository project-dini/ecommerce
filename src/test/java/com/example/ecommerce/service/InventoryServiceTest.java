package com.example.ecommerce.service;

import com.example.ecommerce.model.Product;
import com.example.ecommerce.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 28/01/2024
 */

@SpringBootTest
public class InventoryServiceTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private InventoryService inventoryService;

    @Test
    public void testAddNewProduct(){
        //prepare data
        Product productRequest = new Product( null, "Odol", 12000, 0);
        Product productToAdd = new Product(null, productRequest.getName(), productRequest.getPrice(), productRequest.getQuantity());
        when(productRepository.save(any())).thenReturn(productToAdd);

        //calling the method to be tested
        Product addedProduct = inventoryService.addNewProduct(productRequest);

        //check result
        assertNotNull(addedProduct);
        assertEquals(productRequest.getName(), addedProduct.getName());
        assertEquals(productRequest.getPrice(), addedProduct.getPrice());
        assertEquals(productRequest.getQuantity(), addedProduct.getQuantity());
    }

    @Test
    public void testUpdateProductQuantity(){
        //prepare data
        long productId = 1L;
        int updatedQuantity = 20;
        Product productToUpdate = new Product(productId, "Odol", 12000, 10);
        when(productRepository.findById(productId)).thenReturn(Optional.of(productToUpdate));

        //calling method to be tested
        Product updatedProduct = inventoryService.updateProductQuantity(productId, updatedQuantity);

        //check result
//        assertNotNull(updatedProduct);
//        assertEquals(updatedQuantity, updatedProduct.getQuantity());
    }

    @Test
    public void testCheckProductAvailability(){
        //prepare data
        long productId = 1L;
        Product productToCheck = new Product(productId, "Odol", 12000, 0);
        when(productRepository.findById(productId)).thenReturn(Optional.of(productToCheck));

        //calling method to be tested
        Product checkedProduct = inventoryService.checkProductAvailability(productId);

        //check result
        assertNotNull(checkedProduct);
        assertEquals(productToCheck.getName(), checkedProduct.getName());
        assertEquals(productToCheck.getPrice(), checkedProduct.getPrice());
        assertEquals(productToCheck.getQuantity(), checkedProduct.getQuantity());
    }

    @Test
    public void testOrderProduct(){
        //prepare data
        long productId = 1L;
        int quantityOrdered = 20;
        Product productToOrder = new Product(productId, "Odol", 12000, 50);
        when(productRepository.findById(productId)).thenReturn(Optional.of(productToOrder));

        //calling method to be tested
        boolean orderProduct = inventoryService.orderProduct(productId, quantityOrdered);

        //check result
        assertNotNull(orderProduct);
    }
}
