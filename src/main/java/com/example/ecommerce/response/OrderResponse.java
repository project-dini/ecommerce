package com.example.ecommerce.response;

import lombok.Data;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 28/01/2024
 */

@Data
public class OrderResponse {
    private String status;
    private String message;
    private String productName;
    private int quantityOrdered;
}
