package com.example.ecommerce.response;


import lombok.Data;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 28/01/2024
 */

@Data
public class CommonResponse<T> {
    private String status;
    private String message;
    private T data;
}

