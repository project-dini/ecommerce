package com.example.ecommerce.response;

import com.example.ecommerce.model.Product;
import org.springframework.stereotype.Component;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 28/01/2024
 */

@Component
public class CommonResponseGenerator {
    public <T> CommonResponse<T> successResponse(String message, T data){
        CommonResponse<T> commonResponse = new CommonResponse<>();
        commonResponse.setStatus("200");
        commonResponse.setMessage(message);
        commonResponse.setData(data);

        return commonResponse;
    }

    public <T> OrderResponse successOrder(String message, String productName, int quantityOrdered){
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setStatus("200");
        orderResponse.setMessage(message);
        orderResponse.setProductName(productName);
        orderResponse.setQuantityOrdered(quantityOrdered);

        return orderResponse;
    }
}
