package com.example.ecommerce.controller;

import com.example.ecommerce.model.Product;
import com.example.ecommerce.response.CommonResponse;
import com.example.ecommerce.response.CommonResponseGenerator;
import com.example.ecommerce.response.OrderResponse;
import com.example.ecommerce.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 27/01/2024
 */

@RestController
public class InventoryController {
    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private CommonResponseGenerator responseGenerator;


    @GetMapping("/segitiga")
    public String segiTIga(){
        String segi3 = inventoryService.segiTiga();

        return segi3;
    }

    @PostMapping("/product")
    public ResponseEntity<CommonResponse<Product>> addNewProduct(@RequestBody Product product){
        Product addedProduct = inventoryService.addNewProduct(product);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(responseGenerator.successResponse("Product successfully added", addedProduct));
    }

    @PostMapping("/product/{productId}/quantity")
    public ResponseEntity<CommonResponse<Product>> updateProductQuantity(@PathVariable Long productId, @RequestParam int quantity){
        Product updatedProduct = inventoryService.updateProductQuantity(productId, quantity);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(responseGenerator.successResponse("The product has been successfully updated", updatedProduct));

    }

    @GetMapping("/product/{productId}/availability")
    public ResponseEntity<CommonResponse<Product>> checkProductAvailability(@PathVariable Long productId){
        Product product = inventoryService.checkProductAvailability(productId);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(responseGenerator.successResponse("Product detail", product));
    }

    @PostMapping("/product/{productId}/order")
    public ResponseEntity<OrderResponse> orderProduct(@PathVariable Long productId, @RequestParam int quantityOrdered){
        Product product = inventoryService.checkProductAvailability(productId);
        String productName = product.getName();
        boolean orderSuccess = inventoryService.orderProduct(productId, quantityOrdered);
        return  ResponseEntity.status(HttpStatus.CREATED)
                .body(responseGenerator.successOrder("Product ordered successfully", productName, quantityOrdered));


//        if (orderSuccess){
//            ResponseEntity.status(HttpStatus.CREATED)
//                    .body(responseGenerator.successResponse("Product ordered successfully", true));
////            return ResponseEntity.ok("Product ordered successfully");
//        }else {
//            ResponseEntity.status(HttpStatus.BAD_REQUEST)
//                    .body("Failed to order product");
////            return ResponseEntity.badRequest().body("Failed to order product");
//        }
    }

}
