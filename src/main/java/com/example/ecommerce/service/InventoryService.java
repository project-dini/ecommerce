package com.example.ecommerce.service;

import com.example.ecommerce.model.Product;
import com.example.ecommerce.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 27/01/2024
 */

@Service
public class InventoryService {
    @Autowired
    private ProductRepository productRepository;

    //Add New Product to Inventory
    public Product addNewProduct(Product product){
        return productRepository.save(product);
    }

    //Update Product Quantity
    public Product updateProductQuantity(Long productId, int newQuantity){
        Product product = productRepository.findById(productId).orElse(null);
        if (product != null){
            product.setQuantity(newQuantity);
            return productRepository.save(product);
        }else {
            return null; //Product not found
        }

    }

    //Check Product Availability
    public Product checkProductAvailability(Long productId){
        return productRepository.findById(productId).orElse(null);
    }

    //Order Product
    public boolean orderProduct(Long productId, int quantityOrdered){
        Product product = productRepository.findById(productId).orElse(null);
        if (product != null && product.getQuantity() >= quantityOrdered){
            int updateQuantity = product.getQuantity() - quantityOrdered;
            product.setQuantity(updateQuantity);
            productRepository.save(product);
            return true; //Order succeeded
        }
        return false; //Order failed
    }



    public String segiTiga(){

        String all = "*";
        String lineBreak = "\n";
        String line0 = "";
        String line1 = "";
        String line2 = "";
        String line3 = "";
        String line4 = "";

        for ( var row = 0; row<=4; row++){
            if (row == 0){
                line0 = "*" + lineBreak;
            }
            if (row == 1){
                for (var col = 0; col<=4; col++){
                    if (col<3){
                        line1 = line1 + " ";

                    }else {
                        line1 = line1 + "*";
                    }
                }
                line1 = line1 + lineBreak;
            } else if (row == 2) {
                for (var col = 0; col<=4; col++){
                    if (col<2){
                        line2 = line2 + " ";
                    }else {
                        line2 = line2 + "*";
                    }
                }
                line2 = line2 +  lineBreak;
            } else if (row ==3) {
                for (var col = 0; col<=4; col++){
                    if (col<1){
                        line3 = line3 + " ";
                    }else {
                        line3 = line3 + "*";
                    }
                }
                line3 = line3 + lineBreak;
            }else if (row == 4){
                for (var col = 0; col<=4; col++){
                    line4 = line4 + "*";
                }
            }
        }

        return line0 + line1 + line2 + line3 +line4;
    }
    
}
