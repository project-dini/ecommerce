package com.example.ecommerce.repository;

import com.example.ecommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author adl.dinir [adl.dinir@xl.co.id]
 * created at 27/01/2024
 */

public interface ProductRepository extends JpaRepository<Product, Long> {
}
